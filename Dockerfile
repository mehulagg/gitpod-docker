FROM gitpod/workspace-full-vnc:latest

USER root

RUN apt-get update && apt-get install -y apt-transport-https && \
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt install -y ./google-chrome-stable_current_amd64.deb && \
    apt install -y chromium-browser firefox snapd && \
    snap install chromium
